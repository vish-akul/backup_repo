#!/bin/bash

echo "Credentials: user/user || root/root"

USER=user
if [ $# -ge 1 ]; then
    if [ "$1" == "user" ] || [ "$1" == "root" ]; then
        USER="$1"
        shift
    fi
fi

ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p 22055 ${USER}@127.0.0.1 $*
exit 0
