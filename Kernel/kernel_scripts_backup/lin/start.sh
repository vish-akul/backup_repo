#!/bin/bash
/home/k4iz3n/thekernel/qemu-2.12.0/aarch64-softmmu/qemu-system-aarch64 \
	-machine virt \
	-cpu cortex-a57 \
	-machine type=virt \
	-nographic -smp 1 \
	-m 2048 \
	-redir tcp:5022::22 \
	-kernel ./arch/arm64/boot/Image \
	-append "console=ttyAMA0" \
	$1 $2

