''' how I initially solved the challenge
objdump -d -M intel rev8 | grep "BYTE PTR \[eax\],0x" | cut -d 'x' -f3 | xxd -r -p
'''
import r2pipe

r2 = r2pipe.open("./rev8")

output = r2.cmd("/c mov byte [eax], 0x")

lines = output.splitlines()

final = ''
for i in lines:
    final += chr(int(i.split('x')[3],16))
print final

''' using re
values = [m.start() for m in re.finditer('0x..', output)]

final = ''
for i in values:
    final += chr(int(output[i+2:i+4],16))
print final
'''


