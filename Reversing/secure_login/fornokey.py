import re
import subprocess
from pwn import *
import time

s = 'F05664E983F54E5FA6D5D4FFC5BF930743F60D8FC2C78AFBB0AF7C82664F2043'

key = '1234123412341234123412341234123412341234123412341234123412341234'

#io = remote('securelogin.acebear.site','5001')
io = process('./secure_login')
#gdb.attach(io,'b *0x08048a25')
welcome = io.recvuntil('Current time: ')
the_time = io.recvuntil('2018')[4:]
print the_time
rest_welcome = io.recvuntil('Give me your name:')
pattern = '%b %d %H:%M:%S %Y'
epoch = int(time.mktime(time.strptime(the_time, pattern)))
randoms = subprocess.check_output(['./testsec', str(epoch)]).split('\n')

print randoms
f = []
final = ['','','','','','','','','','','','','','','','']
f = re.findall('.{4}',s)
g = ['0000']
g += f
keys = re.findall('.{4}',key)

code = ''

print keys

for i in range(15,-1,-1):
    final[i] = int(f[i],16) ^ int(g[i],16) ^ int(randoms[i][-4:],16)
    

print final

for i in final:
    code += '%0*X' % (4,i)

print code

io.send('akul')
io.recvuntil('Gime me your password:')
io.send(code)

io.interactive()
