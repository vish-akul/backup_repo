def baseconvert(n, base):
    """convert positive decimal integer n to equivalent in another base (2-36)"""

    digits = "0123456789abcdefghijklmnopqrstuvwxyz"

    try:
        n = int(n)
        base = int(base)
    except:
        return ""

    if n < 0 or base < 2 or base > 36:
        return ""

    s = ""
    while 1:
        r = n % base
        s = digits[r] + s
        n = n / base
        if n == 0:
            break

    return s[-8:].zfill(8)



if __name__ == "__main__":
    
    serial = ""

    print "(enter only 8 characters)"

    name = raw_input("Nom: ")

    firstpart = name[:4]
    secondpart = name[4:]
    secpartint = secondpart[::-1].encode("hex")
    
    firstpart = firstpart[::-1].encode("hex")
    secondpart = baseconvert(int(secondpart[::-1].encode("hex"),16),20)
    thirdpart = baseconvert(int(secpartint,16)+int(firstpart,16),36)

    print "Code: "+firstpart+'-'+secondpart+'-'+thirdpart

    
    
