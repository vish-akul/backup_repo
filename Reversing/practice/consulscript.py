import r2pipe

'''
python consulscript.py 2> /dev/null | grep flag
'''

r2 = r2pipe.open("./consul")
r2.cmd('aaa')
r2.cmd('ood')
r2.cmd('db 0x00400b45')
r2.cmd('dc')
r2.cmd('db 0x00400a76')
r2.cmd('dr rip=0x00400a6a')
r2.cmd('dc')
r2.cmd('db 0x00400a38')
r2.cmd('db 0x00400a2e')
for i in range(70):
    r2.cmd('dr rip=0x00400a0d')
    r2.cmd('dc')
    print(r2.cmd('ps @ rsi'))
    r2.cmd('dc')

r2.quit()
