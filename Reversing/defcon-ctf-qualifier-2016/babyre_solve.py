import angr
import claripy
import logging
import archinfo
logging.getLogger('angr.manager').setLevel('DEBUG')

win = 0x004028e9
lose = 0x00402941

proj = angr.Project("./baby-re")

state = proj.factory.blank_state(addr=0x004028d9)

for x in range(13):
    state.memory.store(state.regs.rbp-0x60+(x*4),claripy.BVS('int{}'.format(x),32))

sm = proj.factory.simulation_manager(state)
sm.explore(find=win,avoid=lose)
found=sm.found[0]

passwd = []

for x in range(13):
    passwd.append(found.solver.eval(found.memory.load(found.regs.rbp-0x60+(x*4),2),cast_to=str))

print ''.join(passwd)
