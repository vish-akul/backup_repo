import claripy
import angr

win = 0x08048b43
lose = 0x08048b3e

project = angr.Project("./bomb",auto_load_libs=False)

inp1 = claripy.BVS('inp1',100*8)

state = project.factory.blank_state(addr=0x08048b29)
state.regs.eax = claripy.BVV(0x804b680,32)
state.memory.store(0x804b680,inp1)

sm = project.factory.simulation_manager(state)
sm.explore(find=win, avoid=lose)
found = sm.found[0]

solution1 = found.solver.eval(inp1, cast_to=str)

print solution1 

