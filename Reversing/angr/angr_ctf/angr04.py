import sys
import angr
import claripy
import logging
import archinfo
logging.getLogger('angr').setLevel('DEBUG')

win = 0x080486e4
lose = 0x080486d2

inp1 = claripy.BVS("inp1",32)
inp2 = claripy.BVS("inp2",32)
proj = angr.Project("./04_angr_symbolic_stack")
        #,auto_load_libs=False)

#state = proj.factory.entry_state(add_options={angr.options.LAZY_SOLVES})
state = proj.factory.blank_state(addr=0x08048697)
        #,add_options={angr.options.LAZY_SOLVES})
state.memory.store(state.regs.ebp-0xc,inp1,endness=archinfo.Endness.LE)
state.memory.store(state.regs.ebp-0x10,inp2,endness=archinfo.Endness.LE)

#for byte in inp1.chop(8):
#    state.add_constraints(byte != '\x00') # null
#    state.add_constraints(byte >= ' ') # '\x20'
#    state.add_constraints(byte <= '~') # '\x7e'

sm = proj.factory.simulation_manager(state)
sm.explore(find=win,avoid=lose)
found=sm.found[0]

print found.solver.eval(inp1),
print found.solver.eval(inp2)
