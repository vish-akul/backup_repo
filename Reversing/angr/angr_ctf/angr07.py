import sys
import angr
import claripy
import logging
import archinfo
logging.getLogger('angr').setLevel('DEBUG')

win = 0x080489b0
lose = 0x08048996

proj = angr.Project("./07_angr_symbolic_file")
        #,auto_load_libs=False)

#state = proj.factory.entry_state(add_options={angr.options.LAZY_SOLVES})
initstate = proj.factory.blank_state(addr=0x080488db)
        #,add_options={angr.options.LAZY_SOLVES})

filename = "OJKSQYDP.txt"
filesize = 64

filemem = angr.state_plugins.SimSymbolicMemory()
filemem.set_state(initstate)

password = claripy.BVS('password', filesize * 8)
filemem.store(0, password)

file_options = 'r'
password_file = angr.storage.SimFile(filename, file_options, content=filemem,size=filesize)

symbolic_filesystem = {
            filename : password_file
}
initstate.posix.fs = symbolic_filesystem


#for byte in inp1.chop(8):
#    state.add_constraints(byte != '\x00') # null
#    state.add_constraints(byte >= ' ') # '\x20'
#    state.add_constraints(byte <= '~') # '\x7e'

sm = proj.factory.simulation_manager(initstate)
sm.explore(find=win,avoid=lose)
found=sm.found[0]

print found.solver.eval(password,cast_to=str)
