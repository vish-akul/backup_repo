import sys
import angr
import claripy
import logging
logging.getLogger('angr').setLevel('DEBUG')

win = 0x080488ef
lose = 0x080488dd

inp1 = claripy.BVS("inp1",32)
inp2 = claripy.BVS("inp2",32)
inp3 = claripy.BVS("inp3",32)
proj = angr.Project("./03_angr_symbolic_registers")
        #,auto_load_libs=False)

#state = proj.factory.entry_state(add_options={angr.options.LAZY_SOLVES})
state = proj.factory.blank_state(addr=0x08048886,add_options={angr.options.LAZY_SOLVES})
state.regs.eax = inp1
state.regs.ebx = inp2
state.regs.edx = inp3

#for byte in inp1.chop(8):
#    state.add_constraints(byte != '\x00') # null
#    state.add_constraints(byte >= ' ') # '\x20'
#    state.add_constraints(byte <= '~') # '\x7e'

sm = proj.factory.simulation_manager(state)
sm.explore(find=win,avoid=lose)
found=sm.found[0]

print found.solver.eval(inp1,cast_to=str).encode("hex"),
print found.solver.eval(inp2,cast_to=str).encode("hex"),
print found.solver.eval(inp3,cast_to=str).encode("hex")
