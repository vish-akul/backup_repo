import sys
import angr
import claripy
import logging
import archinfo
logging.getLogger('angr').setLevel('DEBUG')

win = 0x080489b0
lose = 0x08048996

inp1 = claripy.BVS("inp1",8*8)
proj = angr.Project("./07_angr_symbolic_file")
        #,auto_load_libs=False)

#state = proj.factory.entry_state(add_options={angr.options.LAZY_SOLVES})
state = proj.factory.blank_state(addr=0x0804893c)
        #,add_options={angr.options.LAZY_SOLVES})
state.memory.store(0x804a0a0,inp1)

#for byte in inp1.chop(8):
#    state.add_constraints(byte != '\x00') # null
#    state.add_constraints(byte >= ' ') # '\x20'
#    state.add_constraints(byte <= '~') # '\x7e'

sm = proj.factory.simulation_manager(state)
sm.explore(find=win,avoid=lose)
found=sm.found[0]

print found.solver.eval(inp1,cast_to=str)
