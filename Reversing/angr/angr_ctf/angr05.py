import sys
import angr
import claripy
import logging
logging.getLogger('angr').setLevel('DEBUG')

win = 0x0804866d
lose = 0x0804865b

inp1 = claripy.BVS("inp1",8*8)
inp2 = claripy.BVS("inp2",8*8)
inp3 = claripy.BVS("inp3",8*8)
inp4 = claripy.BVS("inp4",8*8)
proj = angr.Project("./05_angr_symbolic_memory")
        #,auto_load_libs=False)

#state = proj.factory.entry_state(add_options={angr.options.LAZY_SOLVES})
state = proj.factory.blank_state(addr=0x08048601)
        #,add_options={angr.options.LAZY_SOLVES})
state.memory.store(0xa1ba1c0,inp1)
state.memory.store(0xa1ba1c8,inp2)
state.memory.store(0xa1ba1d0,inp3)
state.memory.store(0xa1ba1d8,inp4)

#for byte in inp1.chop(8):
#    state.add_constraints(byte != '\x00') # null
#    state.add_constraints(byte >= ' ') # '\x20'
#    state.add_constraints(byte <= '~') # '\x7e'

sm = proj.factory.simulation_manager(state)
sm.explore(find=win,avoid=lose)
found=sm.found[0]

print found.solver.eval(inp1,cast_to=str),
print found.solver.eval(inp2,cast_to=str),
print found.solver.eval(inp3,cast_to=str),
print found.solver.eval(inp4,cast_to=str)
