import sys
import angr
import claripy
import logging
import archinfo
logging.getLogger('angr').setLevel('DEBUG')

win = 0x0804FC9C
lose = 0x0804FC8A

proj = angr.Project("./11_angr_sim_scanf")
state = proj.factory.entry_state()

sm = proj.factory.simulation_manager(state)
sm.explore(find=win,avoid=lose)
found=sm.found[0]

print found.posix.dumps(sys.stdin.fileno())
