import sys
import angr
import claripy
import logging
logging.getLogger('angr').setLevel('DEBUG')

win = 0x08049980
lose = 0x0804996B

#inp1 = claripy.BVS("inp1",8*8)
proj = angr.Project("./02_angr_find_condition")
        #,auto_load_libs=False)

state = proj.factory.entry_state(add_options={angr.options.LAZY_SOLVES})
#state = proj.factory.blank_state(addr=0x0804860f,add_options={angr.options.LAZY_SOLVES})
#state.memory.store(state.regs.ebp-0x15,inp1)

#for byte in inp1.chop(8):
#    state.add_constraints(byte != '\x00') # null
#    state.add_constraints(byte >= ' ') # '\x20'
#    state.add_constraints(byte <= '~') # '\x7e'

sm = proj.factory.simulation_manager(state)
sm.explore(find=win,avoid=lose)
found=sm.found[0]

print found.posix.dumps(sys.stdin.fileno())
