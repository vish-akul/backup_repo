import sys
import angr
import claripy
import logging
import archinfo
logging.getLogger('angr').setLevel('DEBUG')

win = 0x08048768
lose = 0x08048756

proj = angr.Project("./09_angr_hooks")

state = proj.factory.entry_state()

@proj.hook(0x080486b3, length=5)
def check_equals(state):
    string = state.memory.load(0x804a054,16)
    state.regs.eax = claripy.If(string == "XYMKBKUHNIQYNQXE",claripy.BVV(1,32),claripy.BVV(0,32))

sm = proj.factory.simulation_manager(state)
sm.explore(find=win,avoid=lose)
found=sm.found[0]

print found.posix.dumps(sys.stdin.fileno())
