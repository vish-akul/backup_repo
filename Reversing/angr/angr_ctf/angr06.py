import sys
import angr
import claripy
import logging
import archinfo
logging.getLogger('angr').setLevel('DEBUG')

win = 0x08048759
lose = 0x08048747

inp1 = claripy.BVS("inp1",8*8)
inp2 = claripy.BVS("inp2",8*8)
proj = angr.Project("./06_angr_symbolic_dynamic_memory")
        #,auto_load_libs=False)

#state = proj.factory.entry_state(add_options={angr.options.LAZY_SOLVES})
state = proj.factory.blank_state(addr=0x08048699)
        #,add_options={angr.options.LAZY_SOLVES})
buffer0 = claripy.BVV(0x960c000,32)
buffer1 = claripy.BVV(0x960c010,32)
state.memory.store(0xabcc8a4,buffer0,endness=archinfo.Endness.LE)
state.memory.store(0xabcc8ac,buffer1,endness=archinfo.Endness.LE)
state.memory.store(buffer0,inp1)
state.memory.store(buffer1,inp2)

#for byte in inp1.chop(8):
#    state.add_constraints(byte != '\x00') # null
#    state.add_constraints(byte >= ' ') # '\x20'
#    state.add_constraints(byte <= '~') # '\x7e'

sm = proj.factory.simulation_manager(state)
sm.explore(find=win,avoid=lose)
found=sm.found[0]

print found.solver.eval(inp1,cast_to=str),
print found.solver.eval(inp2,cast_to=str)
