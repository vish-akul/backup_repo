import sys
import angr
import claripy
import logging
import archinfo
logging.getLogger('angr').setLevel('DEBUG')


proj = angr.Project("./10_angr_simprocedures")

win = 0x0804A736

inp1 = claripy.BVS("inp1",16*8)

state = proj.factory.blank_state(addr=0x080486C3)
state.memory.store(state.regs.ebp-0x24,claripy.BVV(0xdeadbeef,32),endness=archinfo.Endness.LE)
state.memory.store(0x804c048,"ORSDDWXHZURJRBDH")
state.memory.store(state.regs.ebp-0x1d,inp1)

sm = proj.factory.simulation_manager(state)
sm.explore(find=win)
found = sm.found[0]

processed = found.memory.load(state.regs.ebp-0x1d,16)

found.add_constraints(processed == "ORSDDWXHZURJRBDH")

print found.solver.eval(inp1,cast_to=str)
