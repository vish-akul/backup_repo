import sys
import angr
import claripy
import logging
import archinfo
logging.getLogger('angr').setLevel('DEBUG')

win = 0x08048673
lose = 0x08048682

inp1 = claripy.BVS("inp1",16*8)
proj = angr.Project("./08_angr_constraints")
        #,auto_load_libs=False)

state = proj.factory.blank_state(addr=0x08048625,add_options={angr.options.LAZY_SOLVES})

#state.memory.store(0x804a040,claripy.BVV(0x44505541,32),endness=archinfo.Endness.LE)
#state.memory.store(0x804a044,claripy.BVV(0x52504e4e,32),endness=archinfo.Endness.LE)
#state.memory.store(0x804a048,claripy.BVV(0x525a454f,32),endness=archinfo.Endness.LE)
#state.memory.store(0x804a04c,claripy.BVV(0x424b574a,32),endness=archinfo.Endness.LE)
state.memory.store(0x804a050,inp1)

#for byte in inp1.chop(8):
#    state.add_constraints(byte != '\x00') # null
#    state.add_constraints(byte >= ' ') # '\x20'
#    state.add_constraints(byte <= '~') # '\x7e'

sm = proj.factory.simulation_manager(state)
sm.explore(find=win,avoid=lose)
found = sm.found[0]

processed = found.memory.load(0x804a050,16)

found.add_constraints(processed == "AUPDNNPROEZRJWKB")

print found.solver.eval(inp1,cast_to=str)
