import sys
import angr
import claripy
import logging
import archinfo
logging.getLogger('angr').setLevel('DEBUG')


def part1():

    win = 0x080486ae

    inp1 = claripy.BVS("inp1",16*8)

    state = proj.factory.blank_state(addr=0x08048665)
    state.memory.store(0x804a054,inp1)

    sm = proj.factory.simulation_manager(state)
    sm.explore(find=win)
    found = sm.found[0]

    processed = found.memory.load(0x804a054,16)

    found.add_constraints(processed == "XYMKBKUHNIQYNQXE")

    return found.solver.eval(inp1,cast_to=str)

def part2():
    win = 0x08048768
    lose = 0x08048756
    
    state = proj.factory.entry_state()

    class checkstring(angr.SimProcedure):
        def run(processed):
            return 1

    proj.hook_symbol('check_equals_XYMKBKUHNIQYNQXE',checkstring())

    sm = proj.factory.simulation_manager(state)
    sm.explore(find=win,avoid=lose)
    found=sm.found[0]

    return found.posix.dumps(sys.stdin.fileno())[16:]

if __name__ == "__main__":
    
    proj = angr.Project("./09_angr_hooks")
    
    pass1 = part1()
    pass2 = part2()

    print pass1
    print pass2
