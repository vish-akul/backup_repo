import sys
import angr
import logging
logging.getLogger('angr').setLevel('DEBUG')

win = 0x08048686
lose = 0x08048698

proj = angr.Project("./12_angr_veritesting")
state = proj.factory.entry_state()

sm = proj.factory.simulation_manager(state, veritesting = True)
sm.explore(find=win,avoid=lose)

found=sm.found[0]

print found.posix.dumps(sys.stdin.fileno())
