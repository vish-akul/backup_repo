from subprocess import call
import claripy
import angr

win = 0x080487f2
lose = 0x0804871b

project = angr.Project("crackme",auto_load_libs=False)

argv1 = claripy.BVS("argv1",100*8)

state = project.factory.entry_state(args=["./crackme",argv1])

sm = project.factory.simulation_manager(state)
sm.explore(find=win,avoid=lose)
found = sm.found[0]

solution = found.solver.eval(argv1, cast_to=str).rstrip(chr(0))

print solution

call(["./crackme", solution])
