import archinfo
import claripy
import angr

win = 0x00400722
lose = 0x00400850


project = angr.Project("./unbreakable-enterprise-product-activation",auto_load_libs=False)

argv1 = claripy.BVS("argv1",67*8)
state = project.factory.entry_state(args=["./unbreakable-enterprise-product-activation",argv1],add_options={angr.options.LAZY_SOLVES})

#state = project.factory.blank_state(addr=0x004005bd)
state.libc.buf_symbolic_bytes=67 + 1
for byte in argv1.chop(8):
    state.add_constraints(byte != '\x00') # null
    state.add_constraints(byte >= ' ') # '\x20'
    state.add_constraints(byte <= '~') # '\x7e'

#state.memory.store(0x6042c0,argv1,endness=archinfo.Endness.LE) # store at address : 0x6042c0 (the input argument, string)

# blank state is created after the strncpy function.

sm = project.factory.simulation_manager(state)
sm.explore(find=win,avoid=lose)
found = sm.found[0]

solution = found.solver.eval(argv1, cast_to=str).rstrip('@')

print solution

