from subprocess import call
import claripy
import angr

win = 0x08048487
lose = 0x0804849f

project = angr.Project("./rev2")

argv1 = claripy.BVS('argv1',31)
argv2 = claripy.BVS('argv2',31)

state = project.factory.blank_state(addr=0x08048477)
state.regs.eax = argv1
state.regs.edx = argv2

sm = project.factory.simulation_manager(state)
sm.explore(find=win, avoid=lose)
found = sm.found[0]

solution1 = found.solver.eval(argv1, cast_to=int)
solution2 = found.solver.eval(argv2, cast_to=int)

print str(solution1) + " " + str(solution2)

call(["./rev2",str(solution1),str(solution2)])
