from Crypto.Cipher import AES

def pad(pt):
    return pt + (16-len(pt)%16)*str(chr(16-len(pt)%16))

def encrypt(pt):
    key = "akulabhilashpill"
    IV = "1234567890123456"
    obj=AES.new(key,AES.MODE_CBC,IV)
    pt = pad(pt)
    ct = obj.encrypt(pt)
    return ct

def decrypt(ct):
    key = "akulabhilashpill"
    IV = "1234567890123456"
    obj=AES.new(key,AES.MODE_CBC,IV)
    pt = obj.decrypt(ct)
    return pt

if __name__ == '__main__':
    pt = raw_input()
    ct = encrypt(pt)
    print ct.encode("hex")
#    print decrypt(ct).encode("hex")
    
