'''
The encrypt() function acts as the server,
hence every call to the encrypt function
is input being passed to the server.

Main function contains commented out code
that immitates the server.

The byteattack() function is the script,
every call to the encrypt function has 
to be changed to input being passed to 
the server for practical use.
'''

import binascii
from Crypto.Cipher import AES

def pad(pt):
    return pt + (16-len(pt)%16)*str(chr(16-len(pt)%16))

def encrypt(pt):
    secret = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK".decode("base64")
    pt = pt+secret
    key = "akulabhilashpill"
    obj = AES.new(key,AES.MODE_ECB)
    pt = pad(pt)
    ct = obj.encrypt(pt)
    return ct

# [ ] funciton for removing padding from decrypted function 
def rmpadding(pt):
    pass

def byteattack():
    dsecret = ''
    original = encrypt('sizecheck')
    for k in range(0,len(original)/16):
        for j in range(15,-1,-1):
            junk = j*"a"
            original = encrypt(junk)
            original1 = original[0+k*16:16+k*16]
            junk1 = junk+dsecret
            for i in range(0,256):
                ct = encrypt(junk1+chr(i))
                ct1 = ct[0+k*16:16+k*16]
                if ct1 == original1:
                    dsecret += chr(i)
    print dsecret

if __name__ == '__main__':
    
    byteattack()

'''    
    pt = raw_input()
    ct = encrypt(pt)
    print ct.encode("hex")
'''
